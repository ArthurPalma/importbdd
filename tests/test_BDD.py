import unittest
import os
from Database import CreateDB,InsertInDB,UpdateLine
import sqlite3


class BDDTestCase(unittest.TestCase):
"""
    CHEMIN_BDD = "tests/bdd.sqlite"
    CHEMIN_FICHIER_CREATION_TABLE = "tests/fichierCreation.txt"

    CONTENU_FICHIER =""" CREATE TABLE IF NOT EXISTS projects (
                                        id integer PRIMARY KEY,
                                        nom text NOT NULL,
                                        prenom text NOT NULL,
                                        annee_naissance text
                                    ); """

    UPDATE_ROW = [(1, 'MAREE', 'Lucas', '1999', )]

    UPDATE_ROWS = [(1, 'PALMA', 'Lucas', '1999'),
                   (2, 'DA SILVA', 'Arthur', '1999'),
                  ]
    
    INSERT_ONE = [(1, 'DA SILVA', 'Lucas', '1999', )]

    REQUETE_UPDATE = "UPDATE projects SET id=?, nom=?, prenom=?, annee_naissance=? WHERE id=?;"

    REQUETE_INSERT = "INSERT INTO projects VALUES (?,?,?,?);"

    INSERT_MANY = [(1, 'DA SILVA', 'Lucas', '1999'),
                   (2, 'PALMA', 'Arthur', '1999'),
                  ]
    def tearDown(self):
      os.remove(BDDTestCase.CHEMIN_BDD)

    def lectureChamps():
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        curseur.execute('PRAGMA table_info(projects)')
        champs = curseur.fetchall()
        listeChamps = []
        for colonne in champs:
            listeChamps.append(colonne[1])
        connexion.close()
        return listeChamps

    def countRow():
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        curseur.execute('SELECT Count(*) FROM projects;')
        ListeNombreLigne = curseur.fetchone()
        nombreLigne = ListeNombreLigne[0]
        connexion.close()
        return nombreLigne

    def verificationLignes():
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        curseur.execute('SELECT * FROM projects;')
        ListeLignes = curseur.fetchall()
        connexion.close()
        return ListeLignes

    def test_insert_row(self):
        CreateDB("testing.sql","test.sql")
        connexion = sqlite3.connect("testing.sql")
        cursor = connexion.cursor()
        InsertInDB("testing.sql",BDDTestCase.INSERT_ONE)
        connexion.commit()
        var = "testing.sql"
        self.assertEqual(var.countRow(),1)
        ListeLignes = BDDTestCase.verificationLignes()
        self.assertEqual(BDDTestCase.INSERT_ONE,ListeLignes)
        connexion.close()

    def test_insert_rows(self):
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        CreateDB(curseur,BDDTestCase.CONTENU_FICHIER)
        self.assertEqual(BDDTestCase.countRow(),0)
        InsertInDB(curseur,BDDTestCase.REQUETE_INSERT,BDDTestCase.INSERT_MANY)
        connexion.commit()
        self.assertEqual(BDDTestCase.countRow(),2)
        ListeLignes = BDDTestCase.verificationLignes()
        self.assertEqual(BDDTestCase.INSERT_MANY,ListeLignes)
        connexion.close()

    def test_update_rows(self):
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        CreateDB(BDDTestCase.CHEMIN_BDD,BDDTestCase.CONTENU_FICHIER)
        InsertInDB(curseur,BDDTestCase.REQUETE_INSERT,BDDTestCase.INSERT_MANY)
        connexion.commit()
        self.assertEqual(BDDTestCase.countRow(),2)
        UpdateLine(curseur,BDDTestCase.REQUETE_UPDATE,BDDTestCase.UPDATE_ROWS,0)
        connexion.commit()
        self.assertEqual(BDDTestCase.countRow(),2)
        ListeLignes = BDDTestCase.verificationLignes()
        self.assertEqual(BDDTestCase.UPDATE_ROWS,ListeLignes)
        connexion.close()

    def test_update_row(self):
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        CreateDB(curseur,BDDTestCase.CONTENU_FICHIER)
        InsertInDB(curseur,BDDTestCase.REQUETE_INSERT,BDDTestCase.INSERT_ONE)
        connexion.commit()
        self.assertEqual(BDDTestCase.countRow(),1)
        UpdateLine(curseur,BDDTestCase.REQUETE_UPDATE,BDDTestCase.UPDATE_ROW,0)
        connexion.commit()
        self.assertEqual(BDDTestCase.countRow(),1)
        ListeLignes = BDDTestCase.verificationLignes()
        self.assertEqual(BDDTestCase.UPDATE_ROW,ListeLignes)
        connexion.close()
    


    def test_CreateDB(self):
        self.assertEqual(os.path.exists(BDDTestCase.CHEMIN_BDD),False)
        connexion = sqlite3.connect(BDDTestCase.CHEMIN_BDD)
        curseur = connexion.cursor()
        CreateDB(curseur,BDDTestCase.CONTENU_FICHIER)
        self.assertEqual(os.path.exists(BDDTestCase.CHEMIN_BDD),True)
        self.assertEqual(BDDTestCase.countRow(),0)
        self.assertEqual(['id', 'nom', 'prenom', 'annee_naissance'],BDDTestCase.lectureChamps())
        connexion.close()

"""