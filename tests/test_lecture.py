import unittest
import os
import csv
from lecture import lectureFichierChaine

class lectureTestCase(unittest.TestCase):

#Définition de constantes pour l'écriture d'un fichier test
    CHEMIN_CSV = "tests/fichier.csv"

    CHAMPS_CSV = ["Nom","Prénom","Date de naissance",]

    DONNEES_CSV = [
        {
            "Nom": "DA SILVA",
            "Prénom": "Lucas",
            "Date de naissance": "24/12/1999",
            },
            {
            "Nom": "PALMA",
            "Prénom":"Arthur",
            "Date de naissance": "01/01/2000",
            }
            ]

#Ecriture du fichier test avant de commencer l'exécution des tests.
def setUp(self):
     with open(lectureTestCase.CHEMIN_CSV, 'w') as sortie:
        csvwriter = csv.DictWriter(
            sortie, fieldnames=lectureTestCase.CHAMPS_CSV, delimiter=';')
        csvwriter.writeheader()
        csvwriter.writerows(lectureTestCase.DONNEES_CSV)

#Supprime le fichier test après avoir effectué les tests unitaires.
def tearDown(self):
    os.remove(lectureTestCase.CHEMIN_CSV)

def test_lectureFichierChaine(self):
    lignes = lectureFichierChaine(lectureTestCase.CHEMIN_CSV,';')
    self.assertIsNotNone(lignes) # Vérification de l'existence du contenu
    self.assertIsIstance(lignes,list) # Vérification du type retourné
    self.assertEqual(3,len(lignes)) # Vérification de la taille
    #Vérifie que les listes sont bien dans un dictionnaire.
    self.assertIsIstance(lignes[0],dict)
    #Vérifie que les champs correspondent bien.
    self.assertListEqual(
        list(lignes[0].keys(), lectureTestCase.CHAMPS_CSV)
    )
    #Vérifie que les données correspondent bien.
    self.assertDictEqual(lignes[0],lectureTestCase.DONNEES_CSV[0])
