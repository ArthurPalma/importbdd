import argparse
import os
import sys
import sqlite3
import csv
import logging
from lecture import lectureFichierChaine

"""
Here are all the functions used to operate the database

"""


# Function used to create the database
def CreateDB(BDD, instructions):
    """
    Create the database by executing the instructions contained in instructions.sql

    Parameters
    ----------
    BDD : str
        The name of the database to create
    instructions : str
        The filepath of the sql file containing the instructions to create the database

    """

    conn = sqlite3.connect(BDD)
    cursor = conn.cursor()
    with open(instructions) as instruct:
        cursor.execute(instruct.read())
    conn.commit()
    conn.close()

# Function used to insert data into the database
def InsertInDB(BDD, cheminfichier):
    """
    Insert the data contained in the CSV file into the database

    Parameters
    ----------
    BDD : str
        The name of the database to connect
    cheminfichier : str
        The filepath of the csv file to open

    """

    conn = sqlite3.connect(BDD)
    cursor = conn.cursor()
    liste = lectureFichierChaine(cheminfichier, ";")
    for ligne in liste:
        cursor.execute('INSERT INTO siv VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ligne)
    conn.commit()
    conn.close()

# Function used to update the database
def UpdateLine(BDD,lineintuple,immatriculation):
    """
    Update the content of the database depending of the CSV file

    Parameters
    ----------
    BDD : str
        The name of the database to connect
    lineintuple : str
        A line of the CSV file contained in a tuple
    immatriculation : str
        the immatriculation' column of the line

    """
    connection = sqlite3.Connection(BDD)
    cursor = connection.cursor()
    m_tuple = lineintuple + (immatriculation,)
    with open('query_update.sql') as query_u:
        q_update = query_u.read()
        cursor.execute(q_update,m_tuple)
        connection.commit()
    connection.close()

# Function that verify if the database contains the data of the CSV file
def VerifyDB(BDD,immatriculation):
    """
    Verify if the line exist in the database

    Parameters
    ----------
    BDD : str
        The name of the database to connect
    immatriculation : str
        the immatriculation' column of the line
    
    """
    
    connection = sqlite3.Connection(BDD)
    cursor = connection.cursor()
    with open('instructions.sql') as query_create:
        request = query_create.read()
        cursor.execute(request)
        connection.commit()    
    to_return = cursor.execute("Select * from siv where immatriculation  = (?)", (immatriculation,)).fetchone() 
    connection.close()
    return to_return

