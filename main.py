import argparse
import os
import sys
from lecture import lectureFichierChaine
from Database import CreateDB,InsertInDB,UpdateLine,VerifyDB
from Argument import Args
import sqlite3
import csv
import logging



if __name__ == "__main__":
    logging.basicConfig(filename='myapp.log', format='%(asctime)s - %(levelname)s:%(message)s', level=logging.DEBUG)
    args = Args()
    # Vérifie que le chemin mène à un fichier valide
    if not os.path.isfile(args.infile):
        logging.error("Le chemin ne mène pas à un fichier valide")
        print("ERROR 101: %s n'est pas un fichier valide !" % args.infile)
        sys.exit(101)
    logging.info('Creation de la base de donnee')
    CreateDB(args.BDD, "instructions.sql")
    with open(args.infile) as csvfile:
        logging.info('Lecture du fichier CSV')
        reader = csv.DictReader(csvfile,delimiter=';')
        for ligne in reader: #Pour chaque ligne du fichier csv
            value = []           
            for entite in reader.fieldnames:
                value.append(ligne[entite])
            logging.info('Verification des valeurs de la base de donnee')
            condition = VerifyDB(args.BDD,ligne['immatriculation'])
            if not condition:# si la ligne n'existe pas
                logging.info('Insertion des valeurs dans la base de donnee')
                InsertInDB(args.BDD, args.infile)
            else:    
                logging.info('Mise à jour des valeurs de la base de donnee')                
                UpdateLine(args.BDD,tuple(value),ligne['immatriculation'])
